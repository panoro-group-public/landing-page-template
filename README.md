# Panoro Landing Page Template

The project is using:

-   **NextJS**
-   **Typescript**
-   **Tailwind**

## How to run

To start the the development server run:

```bash
npm run dev
```

## Style guide

The project is using Prettier for formatting the files.

The `includes` lines should be grouped in the following way:

```js
//= Functions & Modules
// Own
all the imports that are functions/classes/etc that are created by/for this project
// Others
all the imports that are functions/classes/etc from the packages/node_modules

//= Structures & Data
// Own
all the imports that are simple objects/enums that are created by/for this project
// Others
all the imports that are simple objects/enums from the packages/node_modules

//= React components
// Own
all the imports that are React components that are created by/for this project
// Others
all the imports that are React components from the packages/node_modules

//= Style & Assets
// Own
all the imports that are style (css files) and assets (images) that are created by/for this project
// Others
all the imports that are style (css files) and assets (images) from the packages/node_modules
```

## File structure

-   `public` - the public directory for the website
-   `src` - source files directory
    -   `components` - all React components (that are not pages)
        -   `[page_name]` - each specific components should be a directory named after the page name
    -   `pages` - all the pages of the website
        -   `api` - the server requests
    -   `server_utils` - all utilities related to the server/backend
    -   `client_utils` - all utilities related to the client/frontend
    -   `locale` - the locale files

Rules for naming the files and directories:

-   directories should be in **snake_case**
-   React components should be in **PascalCase**
-   function files should be in **camelCase**
